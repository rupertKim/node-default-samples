/**
 * Created by rupertKim on 2016. 10. 24..
 */
var mysql = require('mysql');


module.exports = function (dbName) {
    var connection = mysql.createConnection({
        host : 'hellodevs.org',
        port : 3306,
        user : 'root',
        password : '1234',
        database : dbName
    });
    return connection;

};