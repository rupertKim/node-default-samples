/**
 * Created by yoonsKim on 15. 9. 17..
 */
var connection = require('./mysql-connection')('js');

connection.connect(function (err) {
    if (err) {
        console.error('mysql connection error');
        console.error(err);
        throw err;
    }
    console.log('connected mysql');

});

var dbQuery = {};

dbQuery.getWish = function (callback) {
    var sql = "SELECT * FROM wish";

    connection.query(sql, function (err, rows) {
        callback(err, rows);
    });
};

dbQuery.getWishList = function (start, count, callback) {
    var sql = "SELECT * FROM wish limit ?,?;";
    start = parseInt(start);
    count = parseInt(count);

    connection.query(sql, [start, count], function (err, rows) {
        console.log(err, rows);
        callback(err, rows);

    });
};

dbQuery.postWish = function (param, callback) {
    // var sql = "INSERT INTO `wish` (`name`)VALUES (?);";
    var keyList = [];
    var valueList = [];
    for (var key in param) {
        keyList.push(key);
        valueList.push(param[key]);
    }

    var sql = "INSERT INTO wish (" + keyList + ") VALUES (?);";
    connection.query(sql, valueList, function (err, rows) {
        callback(err, rows);

    });
};

dbQuery.deleteWish = function (idx, callback) {
    var sql = "delete from wish where idx = ?";

    connection.query(sql, [idx], function (err, rows) {
        callback(err, rows);
    });
};

dbQuery.getWishItem = function (wishIdx, callback) {
    var sql = "SELECT wi.* FROM wish_item AS wi LEFT JOIN wish AS w ON w.idx = wi.list_idx WHERE wi.list_idx = ?";

    connection.query(sql, [wishIdx], function (err, rows) {
        callback(err, rows);
    });
};
dbQuery.postWishItem = function (param, callback) {
    var list_idx = param.list_idx;
    console.log(param.item_list);
    var wish_item_list = eval(param.item_list);
    var queryString="";
    for (var idx in wish_item_list) {
        if(idx !=0){
            queryString+=","
        }
        queryString +=
            "('"+ wish_item_list[idx]['name']+"','"
        + wish_item_list[idx]['memo']+"',"
        + wish_item_list[idx]['rate']+","
        + list_idx
        +")";
    }
    var sql = "INSERT INTO wish_item (name,memo,rate,list_idx) VALUES "+queryString;
    connection.query(sql, function (err, rows) {
        console.log(err, rows);
        callback(err, rows);

    });
};
dbQuery.deleteWishItem = function (idx, callback) {

    var sql = "delete from wish_item where idx = ?";
    connection.query(sql, [idx], function (err, rows) {
        callback(err, rows);

    });
};
dbQuery.postAccount = function (param, callback) {
    var keyList = [];
    var valueList = [];
    for (var key in param) {
        if(!param[key]){
            callback(null,{result:false});
            return;
        }
        keyList.push(key);
        valueList.push(param[key]);

    }
    dbQuery.isAccount(param.email,function (err, isAccount) {
        if(isAccount===true){
            callback(null,{result:true});

        }else{

            var sql = "INSERT INTO `user` (" + keyList + ") VALUES ("+getQueryQuestionMark(valueList.length)+");";

            connection.query(sql, valueList, function (insertUserError, rows) {
                callback(insertUserError,{result:true});
            });
        }
    })


}

dbQuery.isAccount = function (email, callback) {
    var sql = "select if(count(*) =1,'true','false') AS result from user where email = ?";
    connection.query(sql, [email], function (err, rows) {
        console.log(err,rows);
        callback(err,rows[0].result);

    });
}

dbQuery.getBoardList = function (major,start,count, callback) {
    var sql = "SELECT * FROM board order by idx desc limit ?,? " ;
    start = parseInt(start) || 0;
    count = parseInt(count) || 10;

    connection.query(sql,[major,start, count], function (err, rows) {
        console.log(err);
        console.log(rows);
        callback(false, rows);
    });
};
dbQuery.postBoard = function (param, callback) {
    var keyList = [];
    var valueList = [];
    for (var key in param) {
        keyList.push(key);
        valueList.push(param[key]);
    }

    var sql = "INSERT INTO `board` (" + keyList + ") VALUES ("+getQueryQuestionMark(valueList.length)+");";

    console.log(sql, valueList);

    connection.query(sql, valueList, function (err, rows) {
        callback(err,rows);

    });
};
dbQuery.deleteBoard = function (idx, callback) {
    var sql = "delete from `board` where idx = ?";

    connection.query(sql, [idx], function (err, rows) {
        callback(err, rows);
    });
}

function getQueryQuestionMark(size) {
    var qMark="";
    for(var i=0;i<size;i++){
        if(i!=0){
            qMark+=",";
        }
        qMark+="?";
    }
    return qMark;
}


module.exports = dbQuery;