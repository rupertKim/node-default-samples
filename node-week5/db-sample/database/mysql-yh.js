/**
 * Created by yoonsKim on 15. 9. 17..
 */
var connection = require('./mysql-connection')('yh');

connection.connect(function (err) {
    if (err) {
        console.error('mysql connection error');
        console.error(err);
        throw err;
    }
    console.log('connected mysql');

});
var dbQuery = {};

dbQuery.getLectureArticle = function (idx, callback) {
    var sql = "SELECT * FROM lecture_board AS b WHERE b.idx = ?";

    connection.query(sql, [idx], function (err, rows) {

        dbQuery.getCommentList(idx,function (err, commentRows) {
            rows[0].commentList = commentRows;
            console.log('---------',rows);
            callback(err, rows);
        });
    });
};
dbQuery.getCommentList = function (idx, callback) {
    var sql = "SELECT c.* FROM board_comment AS c RIGHT JOIN lecture_board AS b ON b.idx=c.board_idx WHERE b.idx = ?";

    connection.query(sql, [idx], function (err, rows) {
        callback(err, rows);
    });
};

dbQuery.getLectureArticleSize = function (callback) {
    var sql = "SELECT count(*) as cnt FROM lecture_board";

    connection.query(sql, function (err, rows) {
        callback(err, rows[0]['cnt']);
    });
};


dbQuery.getLectureArticleList = function (major,start,count, callback) {
    var sql = "SELECT * FROM lecture_board where major=? order by idx desc limit ?,? " ;
    start = parseInt(start) || 0;
    count = parseInt(count) || 10;

    connection.query(sql,[major,start, count], function (err, rows) {
        console.log(err);
        console.log(rows);
        callback(false, rows);
    });
};
dbQuery.postArticle = function (param, callback) {
    // var sql = "INSERT INTO `lecture_board` (`title`,`article`,`major`)VALUES (?,?,?);";
    var keyList = [];
    var valueList = [];
    for (var key in param) {
        keyList.push(key);
        valueList.push(param[key]);
    }

    var sql = "INSERT INTO `lecture_board` (" + keyList + ") VALUES (?,?,?);";
    console.log(sql, valueList);

    connection.query(sql, valueList, function (err, rows) {
        callback(err,rows);

    });
};
dbQuery.deleteArticle = function (idx, callback) {
    var sql = "delete from `lecture_board` where idx = ?";

    connection.query(sql, [idx], function (err, rows) {
        callback(err, rows);
    });
};

dbQuery.deleteArticle = function (idx, callback) {
    var sql = "delete from `lecture_board` where idx = ?";

    connection.query(sql, [idx], function (err, rows) {
        callback(err, rows);

    });
};

dbQuery.postComment = function (param, callback) {
    var keyList = [];
    var valueList = [];
    for (var key in param) {
        keyList.push(key);
        valueList.push(param[key]);
    }

    var sql = "INSERT INTO `board_comment` (" + keyList + ") VALUES (?,?);";
    console.log(sql, valueList);

    connection.query(sql, valueList, function (err, rows) {
        callback(err,rows);

    });
};
dbQuery.postAccount = function (param, callback) {
    var keyList = [];
    var valueList = [];
    for (var key in param) {
        if(!param[key]){
            callback(null,{result:false});
            return;
        }
        keyList.push(key);
        valueList.push(param[key]);

    }
    dbQuery.isAccount(param.email,function (err, isAccount) {
        if(isAccount===true){
            callback(null,{result:true});

        }else{

            var sql = "INSERT INTO `user` (" + keyList + ") VALUES ("+getQueryQuestionMark(valueList.length)+");";

            connection.query(sql, valueList, function (insertUserError, rows) {
                callback(insertUserError,{result:true});
            });
        }
    })


}

dbQuery.isAccount = function (email, callback) {
    var sql = "select if(count(*) =1,'true','false') AS result from user where email = ?";
    connection.query(sql, [email], function (err, rows) {
        console.log(err,rows);
        callback(err,rows[0].result);

    });
}

function getQueryQuestionMark(size) {
    var qMark="";
    for(var i=0;i<size;i++){
        if(i!=0){
            qMark+=",";
        }
        qMark+="?";
    }
    return qMark;
}



module.exports = dbQuery;