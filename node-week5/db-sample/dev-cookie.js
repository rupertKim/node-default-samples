var cookie = require('cookie');

var coky = {};
coky.registAccount = function (response, accountInfo) {
    response.setHeader('Set-Cookie', cookie.serialize('account', String(JSON.stringify(accountInfo)), {
        httpOnly: true,
        maxAge: 60 * 60 * 24 * 7 // 1 week
    }));
};

coky.getAccount = function (req) {
    var accountInfo = cookie.parse(req.headers.cookie || '').account;
    console.log(accountInfo);
    return JSON.parse(accountInfo);
}

module.exports = coky;
