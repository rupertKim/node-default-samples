var express = require('express');
var router = express.Router();
var coky = require('../dev-cookie');
var cookie = require('cookie');
var mysql = require('../database/mysql-yh');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' ,account:coky.getAccount(req)});
});
router.get('/login',function (req, res) {

  res.render('login');
});

router.post('/login', function(req, res, next) {
  mysql.postAccount(req.body,function (err,result) {
    console.log(err,result);

    if(result.result){
      coky.registAccount(res,req.body);
    }

    res.json(result);

  })
});
module.exports = router;
