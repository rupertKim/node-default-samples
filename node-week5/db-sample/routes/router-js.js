var express = require('express');
var mysql = require('../database/mysql-js');
var router = express.Router();

router.get('/wish', function(req, res, next) {
    var start = req.query.start;
    var size = req.query.size;
    mysql.getWish(function (error,rows) {
        console.log(error,rows);
        res.json(rows);
    });
});
router.get('/wish/:idx', function(req, res, next) {
    mysql.getWish(req.params.idx,function (error,rows) {
        console.log(error,rows);
        res.json(rows);
    });
});
router.post('/wish', function(req, res, next) {
    // var param = {name:"name"};
    mysql.postWish(req.body,function (error,rows) {
        console.log(error,rows);
        res.json(rows);
    });

});
router.get('/wish/delete/:idx', function(req, res, next) {

    mysql.deleteWish(req.params.idx,function (error,rows) {
        console.log(error,rows);
        res.json(rows);
    });

});
router.get('/wish/:wishIdx/item/', function(req, res, next) {

    mysql.getWishItem(req.params.wishIdx,function (error,rows) {
        console.log(error,rows);
        res.json(rows);
    });

});
router.post('/wish/:wishIdx', function(req, res, next) {

    var param = req.body;
    param.list_idx = req.params.wishIdx;

    mysql.postWishItem(param,function (error,rows) {
        console.log(error,rows);
        res.json(rows);
    });

});


module.exports = router;
