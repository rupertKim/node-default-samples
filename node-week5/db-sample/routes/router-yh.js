var express = require('express');
var mysql = require('../database/mysql-yh');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

    mysql.getLectureArticleList('idst',req.query.start,req.query.size,function (error,rows) {
        console.log(error,rows);
        mysql.getLectureArticleSize(function (err, maxSize) {
            console.log(maxSize);
            // res.render("yh/board-list",{boardList: rows,start:req.query.start || 0,size:req.query.size || 10, maxSize:maxSize});
            res.json(rows);
        });
    });

});
router.get('/item/:idx', function(req, res, next) {
    mysql.getLectureArticle(req.params.idx,function (error,rows) {
        console.log(error,rows);
        // res.render("yh/board-detail",{article:rows[0]});
        res.json(rows[0]);
    });

});
router.get('/write', function(req, res, next) {
    res.render("yh/writing-post");
});
router.post('/', function(req, res, next) {
    // var param = {title:'ttttt',article:"body",major:"art"};
    mysql.postArticle(req.body,function (error,rows) {
        console.log(error,rows);
        res.redirect('/yh');
    });

});
router.get('/delete/:idx', function(req, res, next) {

    mysql.deleteArticle(req.params.idx,function (error,rows) {
        res.redirect('/yh');
    });

});



module.exports = router;
