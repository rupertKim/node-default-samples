var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


var list = [];
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/input', function(req, res, next) {
  res.render('input', { title: 'Express' });
});
router.post('/input', function(req, res, next) {
  list.push(req.body);

  res.render('result', { body: req.body });

});
router.post('/input-json', function(req, res, next) {
  res.json(req.files);

});
router.get('/input-result', function(req, res, next) {
  res.json(req.query);
});
router.get('/input-list', function(req, res, next) {
  res.render('list',{list:list});
});

module.exports = router;