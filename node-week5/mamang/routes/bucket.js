var express = require('express');

var router = express.Router();
var mysql = require('../database/mysql-js');

/* GET home page. */
router.get('/', function(req, res, next) {

    mysql.getWishList(function (error,rows) {
        console.log(error,rows);
        res.render('bucket', { list:rows });
    });
});
router.post('/', function(req, res, next) {


    mysql.postWish(req.body,function (error,rows) {
        console.log(error,rows);
        res.json(rows.insertId); //DB에 넣기 postWish가 그 역할을 하게 설정됨
    });

});

router.get('/wish/delete/:idx', function(req, res, next) {

    mysql.deleteWish(req.params.idx, function (error, rows) {
        console.log(error, rows);
        res.json(rows);
    });
});

module.exports = router;