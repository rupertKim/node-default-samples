/**
 * Created by user on 2016-10-22.
 */
$(document).ready(function () {


    $('#bt').click(function () {
        insertWish();
    });
    var isInserting = false;
    function insertWish() {
        if(isInserting){
            return;
        }
        isInserting=true;
        var toplus = $('input[name=item_list]').val();
        console.log(toplus);
        $.ajax({
            method: "POST",
            url: "/bucket",
            data: {name: toplus}
        }).done(function (response) {  //post방식으로 /bucket주소로 name(DB에 입력값으로 설정된 것)을 저장한다 (내용=내가 설정한 변수 toplus)
            console.log(response);
            $('#bucketlist').append("<div class='wish-wrap'><span>"+toplus+"</span> <button id='btt-"+response+"' class='remove-btn'>-</button></div>");
            isInserting=false;

        });
    }

    $("body").on("click", ".remove-btn",(function () {
        var btn_idx = $(this).attr("id").replace("btt-","");
        $(this).parent().remove();

        console.log(btn_idx);
        $.ajax({
            method: "GET",
            url: "/bucket/wish/delete/"+btn_idx,
        }).done(function (msg) {
        });
    }));

    $("input[name=item_list]").keyup(function (e) {
        if(e.keyCode === 13){
            insertWish();
            $("input[name=item_list]").val("");
        }else{
            e.preventDefault();
        }
    })
});






