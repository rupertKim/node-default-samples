/**
 * Created by yoonsKim on 15. 9. 17..
 */
var connection = require('./mysql-connection')('js');

connection.connect(function (err) {
    if (err) {
        console.error('mysql connection error');
        console.error(err);
        throw err;
    }
    console.log('connected mysql');

});

var dbQuery = {};

dbQuery.getWish = function (idx, callback) {
    var sql = "SELECT * FROM wish WHERE idx = ?";

    connection.query(sql, [idx], function (err, rows) {
        callback(err, rows);
    });
};

dbQuery.getWishList = function ( callback) {
    var sql = "SELECT * FROM wish;";

    connection.query(sql, function (err, rows) {
        console.log(err, rows);
        callback(err, rows);

    });
};

dbQuery.postWish = function (param, callback) {
    // var sql = "INSERT INTO `wish` (`name`)VALUES (?);";
    var keyList = [];
    var valueList = [];
    for (var key in param) {
        keyList.push(key);
        valueList.push(param[key]);
    }

    var sql = "INSERT INTO wish (" + keyList + ") VALUES (?);";
    connection.query(sql, valueList, function (err, rows) {
        callback(err, rows);

    });
};

dbQuery.deleteWish = function (idx, callback) {
    var sql = "delete from wish where idx = ?";

    connection.query(sql, [idx], function (err, rows) {
        callback(err, rows);
    });
};

dbQuery.getWishItem = function (wishIdx, callback) {
    var sql = "SELECT wi.* FROM wish_item AS wi LEFT JOIN wish AS w ON w.idx = wi.list_idx WHERE wi.list_idx = ?";

    connection.query(sql, [wishIdx], function (err, rows) {
        callback(err, rows);
    });
};
dbQuery.postWishItem = function (param, callback) {
    var list_idx = param.list_idx;
    console.log(param.item_list);
    var wish_item_list = eval(param.item_list);
    var queryString="";
    for (var idx in wish_item_list) {
        if(idx !=0){
            queryString+=","
        }
        queryString +=
            "('"+ wish_item_list[idx]['name']+"','"
        + wish_item_list[idx]['memo']+"',"
        + wish_item_list[idx]['rate']+","
        + list_idx
        +")";
    }
    var sql = "INSERT INTO wish_item (name,memo,rate,list_idx) VALUES "+queryString;
    connection.query(sql, function (err, rows) {
        console.log(err, rows);
        callback(err, rows);

    });
};
dbQuery.deleteWishItem = function (idx, callback) {

    var sql = "delete from wish_item where idx = ?";
    connection.query(sql, [idx], function (err, rows) {
        callback(err, rows);

    });
};


module.exports = dbQuery;