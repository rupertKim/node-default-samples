
// express의 router를 호출하고 아래에서 만들 router들을 넣어줍니다.
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var name  = req.query.name;
  res.render('index', { title: '공대 7호관',list:['a','b','c'],name: name});
});

/* GET home page. */
router.get('/hi', function(req, res, next) {
  res.render('hi', { title: 'let me show you',message:"hello you guys" });
});
router.get('/myinfo/:id', function(req, res, next) {
  var id = req.params.id;
  res.render('hi', { title: 'let me show you',id:id});
});

router.get('/my/gallery', function(req, res, next) {
  res.render('gallery', { title: 'let me show you my gallery',
    img_list:[
    "http://musicmeta.phinf.naver.net/album/000/429/429818.jpg?type=r300Fll&v=20140516143301",
    "http://musicmeta.phinf.naver.net/album/000/408/408980.jpg?type=r300Fll&v=20131223095827",
    "http://musicmeta.phinf.naver.net/album/000/397/397932.jpg?type=r300Fll&v=20131008180623",
    "http://musicmeta.phinf.naver.net/album/000/319/319680.jpg?type=r300Fll&v=20120511003850",
    "http://musicmeta.phinf.naver.net/album/000/301/301696.jpg?type=r300Fll&v=20120213101306",
    "http://musicmeta.phinf.naver.net/album/000/190/190154.jpg?type=r300Fll&v=20120111171849",
    "http://musicmeta.phinf.naver.net/album/000/184/184117.jpg?type=r300Fll&v=20120126165649"] });
});


//내가 만든 router를 router 변수에 넣어서 모듈에 담습니다.
module.exports = router;
