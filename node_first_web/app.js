//모듈 세팅
var fs = require('fs');//file system
var http = require('http');
var url = require('url');


//웹서버를 세팅하여 변수에 담는다
var web = http.createServer(function(request,response){


    //이 영역은 서버가 호출될때마다 실행됨
    console.log('entered new user!!');

    fs.readFile('web.html',function(error,data){

        response.writeHead(200,{'Content-Type':'text/html'});
        response.end(data);
    });
});


//세팅된 웹서버를 3000 port 에 실행시킨다.
//function 내부에서는 실행된 후에 동작할 기능을 코딩
web.listen(3000,function(){
    console.log('start web server');
});