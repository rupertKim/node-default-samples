var express = require('express');
var router = express.Router();
var list = [];
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

//같은 /input경로임에도 get이랑 post일떄 하는 역할이 나뉘어 진다.
router.get('/input', function(req, res, next) {
  res.render('input', { title: 'Express' });
});
router.post('/input', function(req, res, next) {
  list.push(req.body);

  res.render('result', { body: req.body });

});
//post 등록할때, 데이터가 body로 온다
router.post('/input-json', function(req, res, next) {
  res.json(req.body);

});
//get으로 요청할때, 데이터가 query로 온다
router.get('/input-result', function(req, res, next) {

  res.json(req.query);
});
router.get('/input-list', function(req, res, next) {
  res.render('list',{list:list});
});

module.exports = router;
